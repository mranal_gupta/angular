import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HostelInfoComponent } from "./hostel-info/hostel-info.component";
import { StudentInfoComponent } from "./student-info/student-info.component";
import { ParentInfoComponent } from "./parent-info/parent-info.component";

const routes: Routes = [
  {
    path: "hostel",
    component: HostelInfoComponent
  },
  {
    path: "student",
    component: StudentInfoComponent
  },
  {
    path: "parentinfo",
    component: ParentInfoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
