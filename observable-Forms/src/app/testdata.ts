import { InMemoryDbService } from "angular-in-memory-web-api";

export class TestData implements InMemoryDbService {
  createDb() {
    let bookDetails = [
      { id: 100, name: "Angular by me", category: "angular" },
      { id: 101, name: "jamajham by you", category: "angular" },
      { id: 102, name: "ngrx by they", category: "angular" }
    ];
    return {books:bookDetails};
  }
  
}
