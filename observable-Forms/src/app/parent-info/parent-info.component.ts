import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators, NgForm } from "@angular/forms";

@Component({
  selector: "app-parent-info",
  templateUrl: "./parent-info.component.html",
  styleUrls: ["./parent-info.component.css"]
})
export class ParentInfoComponent implements OnInit {
  forms: FormGroup;
  name: string = "";
  phone: string = "";

  constructor(private formsbuilder: FormBuilder) {
    console.log(formsbuilder);

    this.forms = formsbuilder.group({
      name: ["", Validators.required, Validators.maxLength[10]],
      phone: ["", Validators.required]
    });
  }

  ngOnInit() {}

  clickable(forms: NgForm) {
    console.log(forms.controls);
  }
}