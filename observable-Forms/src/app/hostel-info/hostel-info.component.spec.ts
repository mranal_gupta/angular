import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HostelInfoComponent } from './hostel-info.component';

describe('HostelInfoComponent', () => {
  let component: HostelInfoComponent;
  let fixture: ComponentFixture<HostelInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HostelInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HostelInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
