import { Component, OnInit } from "@angular/core";
import { User } from "../User";
import { NgForm } from "@angular/forms";
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"],
  animations:[
    trigger('myAwesomeAnimation',[
      state('small',style({
        transform:'scale(1)',
      })),
      state('large',style({
        transform:'scale(1.2)',
      })),
      transition('small <=> larger',animate('300ms ease-in',keyframes([
        style({opacity:0,transform:'translateY(-75%)',offset:0}),
        style({opacity:1,transform:'translateY(35px)',offset:.5}),
        style({opacity:1,transform:'translateY(-75%)',offset:1}),
      ]))),
    ]),
  ]
})
export class HomeComponent implements OnInit {


  State:string = 'small'
  
  city = "Delhi ";
  //Pipe Multi 
  dob = new Date(1996, 25, 6);
  salary: number = 65000;

  company: any;
  user = new User();
  constructor() {
   setTimeout(() => {
      this.company = {
        Name: "Sahosoft Solutions",
        City: "Noida",
        State: "UP"
      };
    }, 200);
  }

  animateMe(){
    this.State = (this.State === 'small' ? 'large' : 'small');
  }


  onformSubmit() {
    console.log(this.user);
  }

  reset(form: NgForm) {
    this.user = new User();
    form.reset({
      married: false
    });
  }

  setDefaultValues() {
    this.user.username = "Mranal-Gupta";
    this.user.gender = "male";
    this.user.isMarried = false;
    this.user.isTCAccepted = true;
  }

  click() {
    this.city = "Noida";
  }

  ngOnInit() {}
}
