import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, } from '@angular/forms';
import { Router } from '@angular/router';
import {DataService} from '../data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  rForm: FormGroup;
  username: string;
  password: string;
  titleAlert = 'Enter value';

  constructor(private fb: FormBuilder, private routes: Router, private service: DataService) {
    this.rForm = fb.group({
      username: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });
  } msg;

  ngOnInit() {
  }
  moveToRegister() {
    this.routes.navigate(['/Registration']);
  }

  login() {
    if (!this.rForm.valid) {
      console.log('invalid Form'); return;
    }
    this.service.login(JSON.stringify(this.rForm.value))
      .subscribe(
        data => {  console.log(data);
          const v = JSON.stringify( data );
       if (JSON.parse(v).status === '1') {
          this.routes.navigate(['/Dashboard']);
        } else {
      alert('invalid');
        }
      });
  }
//   check(username: string, password: string) {
//     const output = this.service.checkusernameandpassword(username,password);
//     if (output === true) {
//       this.routes.navigate(['/Ticket']);
//     } else {
//       this.msg = 'Invalid username or password';
//     }
//   }

}
