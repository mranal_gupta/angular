import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Issue} from './models/issue';
import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import 'rxjs/Rx';

@Injectable()
export class DataService {

  private readonly API_URL = 'http://172.24.125.158:3000/routes/';

  dataChange: BehaviorSubject<Issue[]> = new BehaviorSubject<Issue[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  toasterService: any;

  constructor (private httpClient: HttpClient) {}

  get data(): Issue[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  /** CRUD METHODS */
  getAllIssues() {
    const headers = new HttpHeaders();
  headers.append('Content-Type', 'application/json');
  return this.httpClient.get<Issue[]>(this.API_URL).subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
      });
  }
//  REAL LIFE CRUD Methods I've used in my projects. ToasterService uses Material Toasts for displaying messages:


    // ADD, POST METHOD
    addItem(kanbanItem: any): void {
      this.httpClient.post(this.API_URL, kanbanItem).subscribe(data => {
        this.dialogData = kanbanItem;
        this.toasterService.showToaster('Successfully added', 3000);
        },
        (err: HttpErrorResponse) => {
        this.toasterService.showToaster('Error occurred. Details: ' + err.name + ' ' + err.message, 8000);
      });
     }

      // UPDATE, PUT METHOD
    updateItem(kanbanItem: any): void {
      this.httpClient.put(this.API_URL + kanbanItem._id, kanbanItem).subscribe(data => {
          this.dialogData = kanbanItem;
          this.toasterService.showToaster('Successfully edited', 3000);
        },
        (err: HttpErrorResponse) => {
          this.toasterService.showToaster('Error occurred. Details: ' + err.name + ' ' + err.message, 8000);
        });
    }

    // DELETE METHOD
      deleteItem(_id: number): void {
        this.httpClient.delete(this.API_URL + _id).subscribe(data => {
          console.log(data['']);
            this.toasterService.showToaster('Successfully deleted', 3000);
          },
          (err: HttpErrorResponse) => {
            this.toasterService.showToaster('Error occurred. Details: ' + err.name + ' ' + err.message, 8000);
          }
          );
        }



  // tslint:disable-next-line:member-ordering
  issue: any;
  Dataload(issue) {
  const headers = new HttpHeaders();
  headers.append('Content-Type', 'application/json');
  return this.httpClient.get('http://172.24.125.158:3000/routes/issue', { headers: headers })
  .map(res => res);
  }
  // tslint:disable-next-line:member-ordering
  category: any;
  loadData(category) {
  const headers = new HttpHeaders();
  headers.append('Content-Type', 'application/json');
  return this.httpClient.get('http://172.24.125.158:3000/routes/get', { headers: headers })
  .map(res => res);
  }
  // tslint:disable-next-line:member-ordering
  assigne: any;
  load(assigne) {
  const headers = new HttpHeaders();
  headers.append('Content-Type', 'application/json');
  return this.httpClient.get('http://172.24.125.158:3000/routes/assigne', { headers: headers })
  .map(res => res);
  }
  // tslint:disable-next-line:member-ordering
  reporte: any;
  Rload(reporte) {
  const headers = new HttpHeaders();
  headers.append('Content-Type', 'application/json');
  return this.httpClient.get('http://172.24.125.158:3000/routes/reporte', { headers: headers })
  .map(res => res);
  }
register(body: any) {
  return this.httpClient.post('http://172.24.125.158:3000/routes/signup', body, {
    observe: 'body',
    headers: new HttpHeaders().append('Content-type', 'application/json')
  });
}
login(body: any) {
  return this.httpClient.post('http://172.24.125.158:3000/routes/login', body, {
    observe: 'body',
    headers: new HttpHeaders().append('Content-type', 'application/json')
  });
}
}

// checkusernameandpassword(username: string, password: string) {
//   if (username === 'admin' && password === 'admin123') {
//     localStorage.setItem(username, password);
//     return true;
//   } else {
//     return false;
//   }
// }login






