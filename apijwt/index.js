const express = require("express");
const app = express();
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const postRoutes = require("./routers/post");
const fileupload = require("express-fileupload");


//Import Routes
const authRoute = require("./routers/auth");

// For env Config.
dotenv.config();

//Connect to DB
mongoose.connect(
  process.env.DB_CONNECT,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true
  },
  () => console.log("Connected to DB!!")
);

//Middleware
app.use(express.json());
app.use(fileupload());

//Router Middlewares
app.use("/api/user", authRoute);
app.use("/api/posts", postRoutes);

app.listen(2000, () => console.log("Server Up and Running on port 2000"));
